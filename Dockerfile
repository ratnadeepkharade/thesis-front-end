FROM node:13-alpine

WORKDIR /app

COPY package.json package-lock.json ./

#RUN npm install --production

COPY . .

COPY node_modules/ ./node_modules/

EXPOSE 3000

CMD node index.js
