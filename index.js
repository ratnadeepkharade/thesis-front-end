const path = require('path');
const express = require('express')
const axios = require('axios');

const app = express();

app.use('/', express.static(path.join(__dirname, 'public')))

app.get('/api', (req, res) => {
    res.send(process.env.BACKEND_URL || 'localhost')
});

app.get('/date', (req, res) => {
    axios.get('http://' + process.env.BACKEND_URL + ':8080/date')
        .then(response => {
            console.log(response.data)
            res.send(response.data)
        })
        .catch(error => {
            console.log(error);
            res.send(error)
        });
});

app.listen(process.env.PORT || 3000, () => {
    console.log(`App listening on port ${process.env.PORT || 3000}!`)
});
